<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PekerjaanController;
use App\Http\Controllers\API\KeluargaApiController;
use App\Http\Controllers\API\PekerjaanApiController;
use App\Http\Controllers\API\PelatihanApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('keluarga')->group(function () {
    Route::get('/', [KeluargaApiController::class, 'index']);
    Route::post('/', [KeluargaApiController::class, 'store']);
});

Route::prefix('pekerjaan')->group(function () {
    Route::get('/', [PekerjaanApiController::class, 'index']);
    Route::post('/', [PekerjaanApiController::class, 'store']);
    Route::get('/{id}', [PekerjaanApiController::class, 'show']);
    Route::put('/{id}', [PekerjaanApiController::class, 'update']);
    Route::delete('/{id}', [PekerjaanApiController::class, 'destroy']);
});

<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GajiController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\BidangController;
use App\Http\Controllers\SocialController;
use App\Http\Controllers\KaryawanController;
use App\Http\Controllers\KeluargaController;
use App\Http\Controllers\PekerjaanController;
use App\Http\Controllers\PelatihanController;
use App\Http\Controllers\PendidikanController;
use App\Http\Controllers\SertifikasiController;
use App\Http\Controllers\ProfileController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
// Auth login
Auth::routes();
Route::get('/auth/redirect', [SocialController::class, "redirect"])->name('google.redirect');
Route::get('/google/redirect', [SocialController::class, "googleCallback"])->name('google.callback');

Route::get('/', function () {
    return view('welcome');
});

Route::middleware('auth')->group(function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    Route::prefix('/keluarga')->group(function () {
        Route::get('/', [KeluargaController::class, 'index'])->name('keluarga.index');
        Route::post('/', [KeluargaController::class, 'store'])->name('keluarga.store');
        Route::get('/{keluarga_id}/edit', [KeluargaController::class, 'edit'])->name('keluarga.edit');
        Route::put('/{keluarga_id}', [KeluargaController::class, 'update'])->name('keluarga.update');
        Route::get('/{keluarga_id}/delete', [KeluargaController::class, 'delete'])->name('keluarga.delete');
    });

    Route::prefix('/pendidikan')->group(function () {
        Route::get('/', [PendidikanController::class, 'index'])->name('pendidikan.index');
        Route::post('/', [PendidikanController::class, 'store'])->name('pendidikan.store');
        Route::get('/{pendidikan_id}/edit', [PendidikanController::class, 'edit'])->name('pendidikan.edit');
        Route::put('/{pendidikan_id}', [PendidikanController::class, 'update'])->name('pendidikan.update');
        Route::get('/{pendidikan_id}/delete', [PendidikanController::class, 'delete'])->name('pendidikan.delete');
    });

    Route::prefix('/pekerjaan')->group(function () {
        Route::get('/', [PekerjaanController::class, 'index'])->name('pekerjaan.index');
        Route::post('/', [PekerjaanController::class, 'store'])->name('pekerjaan.store');
        Route::get('/{pekerjaan_id}/edit', [PekerjaanController::class, 'edit'])->name('pekerjaan.edit');
        Route::put('/{pekerjaan_id}', [PekerjaanController::class, 'update'])->name('pekerjaan.update');
        Route::get('/{pekerjaan_id}/delete', [PekerjaanController::class, 'delete'])->name('pekerjaan.delete');
    });

    Route::prefix('/pelatihan')->group(function () {
        Route::get('/', [PelatihanController::class, 'index'])->name('pelatihan.index');
        Route::post('/', [PelatihanController::class, 'store'])->name('pelatihan.store');
        Route::get('/{pelatihan_id}/edit', [PelatihanController::class, 'edit'])->name('pelatihan.edit');
        Route::put('/{pelatihan_id}', [PelatihanController::class, 'update'])->name('pelatihan.update');
        Route::get('/{pelatihan_id}/delete', [PelatihanController::class, 'delete'])->name('pelatihan.delete');
    });

    Route::prefix('/sertifikasi')->group(function () {
        Route::get('/', [SertifikasiController::class, 'index'])->name('sertifikasi.index');
        Route::post('/', [SertifikasiController::class, 'store'])->name('sertifikasi.store');
        Route::get('/{sertifikasi_id}/edit', [SertifikasiController::class, 'edit'])->name('sertifikasi.edit');
        Route::put('/{sertifikasi_id}', [SertifikasiController::class, 'update'])->name('sertifikasi.update');
        Route::get('/{sertifikasi_id}/delete', [SertifikasiController::class, 'delete'])->name('sertifikasi.delete');
    });

    Route::get('profile-karyawan', [ProfileController::class, 'indexKaryawan']);
    Route::post('profile-karyawan/update', [ProfileController::class, 'updateKaryawan']);
});

Route::middleware(['admin'])->group(function () {
    // Admin Dashboard
    Route::get('/admin/dashboard', [App\Http\Controllers\HomeController::class, 'admin']);

    // Bidang Pekerjaan
    Route::prefix('/bidang')->group(function () {
        Route::get('/', [BidangController::class, 'index'])->name('bidang.index');
        Route::post('/', [BidangController::class, 'store'])->name('bidang.store');
        Route::get('/{bidang_id}/edit', [BidangController::class, 'edit'])->name('bidang.edit');
        Route::put('/{bidang_id}', [BidangController::class, 'update'])->name('bidang.update');
        Route::get('/{bidang_id}/delete', [BidangController::class, 'delete'])->name('bidang.delete');
    });

    // sertifikasi Admin
    Route::get('sertifikasi/show', [SertifikasiController::class, 'show'])->name('sertifikasi.show');

    // Pelatihan Admin
    Route::get('pelatihan/show', [PelatihanController::class, 'show'])->name('pelatihan.show');

    // Pekerjaan Admin
    Route::get('pekerjaan/show', [PekerjaanController::class, 'show'])->name('pekerjaan.show');

    // Gaji Admin
    Route::get('gaji', [PekerjaanController::class, 'gaji'])->name('pekerjaan.gaji');
    Route::get('gaji/{gaji_id}/edit', [PekerjaanController::class, 'editGaji'])->name('pekerjaan.editGaji');
    Route::put('gaji/{gaji_id}', [PekerjaanController::class, 'updateGaji'])->name('pekerjaan.updateGaji');
    Route::get('gaji/{gaji_id}/delete', [PekerjaanController::class, 'deleteGaji'])->name('pekerjaan.deleteGaji');

    // Pendidikan Admin
    Route::get('pendidikan/show', [PendidikanController::class, 'show'])->name('pendidikan.show');

    // Keluarga Admin
    Route::get('keluarga/show', [KeluargaController::class, 'show'])->name('keluarga.show');

    // Karyawan Admin
    Route::get(
        '/karyawan',
        [KaryawanController::class, 'index']
    )->name('index');

    Route::get('profile-admin', [ProfileController::class, 'indexAdmin']);
    Route::post('profile-admin/update', [ProfileController::class, 'updateAdmin']);
});

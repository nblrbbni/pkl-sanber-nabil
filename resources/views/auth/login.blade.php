@extends('layouts.app')

@section('content')
    <div class="bg" style="background-image: url('{{ asset('assets/images/bg-login.jpg') }}');"></div>
    <div class="contents">

        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-md-12">
                    <div class="form-block mx-auto">
                        <div class="text-center mb-5">
                            <h3 class="text-uppercase">Log In to <strong>EMPAPP</strong></h3>
                        </div>

                        <form method="POST" action="{{ route('login') }}">
                            @csrf

                            <div class="form-group first">
                                <label class="form-label" for="email">{{ __('Email Address') }}</label>
                                <input id="email" type="email"
                                    class="form-control @error('email') is-invalid @enderror" name="email"
                                    value="{{ old('email') }}" required autocomplete="email" autofocus
                                    placeholder="Create an account with email login@admin.com">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group last mb-3">
                                <label class="form-label" for="password">Password</label>
                                <input id="password" type="password"
                                    class="form-control @error('password') is-invalid @enderror" name="password" required
                                    autocomplete="current-password" placeholder="To login as admin">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="d-sm-flex mb-5 align-items-center">
                                <label class="control control--checkbox mb-3 mb-sm-0 form-check-label"><span
                                        class="caption">Remember
                                        me</span>
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember"
                                        checked="checked" {{ old('remember') ? 'checked' : '' }}>
                                    <div class="control__indicator"></div>
                                </label>
                                <span class="ml-auto">
                                    @if (Route::has('password.request'))
                                        <a class="forgot-pass" href="{{ route('password.request') }}">
                                            {{ __('Forgot Your Password?') }}
                                        </a>
                                    @endif
                                </span>
                            </div>

                            <input type="submit" value="Log In" class="btn btn-block py-2 btn-primary">

                            <span class="text-center my-3 d-block">or</span>

                            <div class="">
                                <a href="{{ route('google.redirect') }}" class="btn btn-block py-2 btn-google"><span
                                        class="icon-google mr-3"></span> Login with Google</a>
                            </div>
                        </form>

                        <div class="text-center mt-3">
                            Don't have an account? <a href="/register">Register</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@extends('layout.admin.master')

@section('header')
    <strong>Update Data Bidang Pekerjaan</strong>
@endsection

@section('form')
    <form action="/bidang/{{ $bidang->id }}" method="POST">
        @csrf
        @method('PUT')
        <div class="mb-3">
            <label for="namaBidang" class="form-label">Nama Bidang</label>
            <input type="text" class="form-control" name="namaBidang" id="namaBidang" value="{{ $bidang->namaBidang }}">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection

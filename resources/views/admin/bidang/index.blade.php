@extends('layout.admin.master')

@section('header')
    <strong>Data Bidang Pekerjaan</strong>
@endsection

@section('judul')
    <h5>Tabel Data Bidang Pekerjaan</h5>
@endsection

@section('form')
    <form action="/bidang" method="POST" class="mb-3">
        @csrf
        <div class="mb-3">
            <label for="namaBidang" class="form-label">Nama Bidang</label>
            <input type="text" class="form-control" name="namaBidang" id="namaBidang">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection

@section('content')
    <div class="table-responsive">
        <table class="table table-hover" id="dataTable">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama Bidang</th>
                    <th scope="col">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($bidang as $key => $value)
                    <tr>
                        <th>{{ $key + 1 }}</th>
                        <td>{{ $value->namaBidang }}</td>
                        <td>
                            <form action="/bidang/{{ $value->id }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <a href="/bidang/{{ $value->id }}/edit" class="btn btn-warning">
                                    <i class="fas fa-edit"></i>
                                </a>
                                <a href="/bidang/{{ $value->id }}/delete" class="btn btn-danger"
                                    onclick="confirmation(event)">
                                    <i class="fas fa-trash-alt"></i>
                                </a>
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="3" class="text-center">Tidak Ada Data.</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
@endsection

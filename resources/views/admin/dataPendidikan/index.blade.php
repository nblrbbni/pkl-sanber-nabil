@extends('layout.admin.master')

@section('header')
    <strong>Data Pendidikan Karyawan</strong>
@endsection

@section('judul')
    <h5>Tabel Data Pendidikan Karyawan</h5>
@endsection

@section('form')
    <div class="table-responsive">
        <table class="table table-hover" id="dataTable">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Sekolah Dasar</th>
                    <th scope="col">Sekolah Menengah Pertama</th>
                    <th scope="col">Sekolah Menengah Atas</th>
                    <th scope="col">Perguruan Tinggi</th>
                    <th scope="col">Nama Karyawan</th>
                    <th scope="col">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($pendidikan as $key => $value)
                    <tr>
                        <th>{{ (int) $key + 1 }}</th>
                        <td>{{ $value->sd }}</td>
                        <td>{{ $value->smp }}</td>
                        <td>{{ $value->sma }}</td>
                        <td>{{ $value->kuliah }}</td>
                        <td>
                            {{ DB::table('users')->where('id', $value->user_id)->value('name') }}
                        </td>
                        <td>
                            <form action="/pendidikan/{{ $value->id }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <a href="/pendidikan/{{ $value->id }}/delete" class="btn btn-danger"
                                    onclick="confirmation(event)">Delete</a>
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="7" class="text-center">Tidak Ada Data.</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
@endsection

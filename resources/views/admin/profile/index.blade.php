@extends('layout.admin.master')

@section('header')
    <strong>Profile Admin</strong>
@endsection

@section('judul')
    <h5>Profile</h5>
@endsection

@section('form')
    <form action="/profile-admin/update" method="POST">
        @csrf
        <div class="mb-3">
            <label for="name" class="form-label">Nama Karyawan</label>
            <input type="text" class="form-control" name="name" id="name" value="{{ $user->name }}">
        </div>
        <div class="mb-3">
            <label for="email" class="form-label">Nama Penyelenggara</label>
            <input type="text" class="form-control" name="email" id="email" value="{{ $user->email }}">
        </div>
        <div class="mb-3">
            <label for="password" class="form-label">Keahlian</label>
            <input type="password" class="form-control" name="password" id="password" placeholder="Ganti Password Anda disini!">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection

@extends('layout.admin.master')

@section('header')
    <strong>Data Sertifikasi Karyawan</strong>
@endsection

@section('judul')
    <h5>Tabel Data Sertifikasi Karyawan</h5>
@endsection

@section('form')
    <div class="table-responsive">
        <table class="table table-hover" id="dataTable">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Sertifikat</th>
                    <th scope="col">Nama Penyelenggara</th>
                    <th scope="col">Tahun Sertifikat</th>
                    <th scope="col">Nama Karyawan   </th>
                    <th scope="col">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($sertifikasi as $key => $value)
                    <tr>
                        <th>{{ $key + 1 }}</th>
                        <td>{{ $value->sertifikat }}</td>
                        <td>{{ $value->penyelenggara }}</td>
                        <td>{{ $value->tahun }}</td>
                        <td>
                            {{ DB::table('users')->where('id', $value->user_id)->value('name') }}
                        </td>
                        <td>
                            <form action="/sertifikasi/{{ $value->id }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <a href="/sertifikasi/{{ $value->id }}/delete" class="btn btn-danger"
                                    onclick="confirmation(event)">Delete</a>
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="6" class="text-center">Tidak Ada Data.</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
@endsection

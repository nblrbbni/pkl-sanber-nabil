@extends('layout.admin.master')

@section('header')
    <strong>Data Pekerjaan Karyawan</strong>
@endsection

@section('judul')
    <h5>Tabel Data Pekerjaan Karyawan</h5>
@endsection

@section('form')
    <div class="table-responsive">
        <table class="table table-hover" id="dataTable">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama Karyawan</th>
                    <th scope="col">Nama Pekerjaan</th>
                    <th scope="col">Gaji Karyawan</th>
                    <th scope="col">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($pekerjaan as $key => $value)
                    <tr>
                        <th>{{ $key + 1 }}</th>
                        <td>{{ $value->namaKaryawan }}</td>
                        <td>{{ $value->namaPekerjaan }}</td>
                        <td>Rp. {{ $value->gaji }}</td>
                        <td>
                            <form action="/gaji/{{ $value->id }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <a href="/gaji/{{ $value->id }}/edit" class="btn btn-warning">
                                    <i class="fas fa-edit"></i>
                                </a>
                                <a href="/gaji/{{ $value->id }}/delete" class="btn btn-danger"
                                    onclick="confirmation(event)">
                                    <i class="fas fa-trash-alt"></i>
                                </a>
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="4" class="text-center">Tidak Ada Data.</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
@endsection

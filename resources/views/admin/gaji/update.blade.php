@extends('layout.admin.master')

@section('header')
    <strong>Update Data Gaji Karyawan</strong>
@endsection

@section('form')
    <form action="/gaji/{{ $pekerjaan->id }}" method="POST">
        @csrf
        @method('PUT')
         <div class="mb-3">
            <label for="namaKaryawan" class="form-label">Nama Karyawan</label>
            <input type="text" class="form-control" name="namaKaryawan" value="{{ $pekerjaan->namaKaryawan }}" readonly>
        </div>
        <div class="mb-3">
            <label for="namaPekerjaan" class="form-label">Nama Pekerjaan</label>
            <select name="namaPekerjaan" id="namaPekerjaan" class="form-control" required>
                @foreach ($bidang as $value)
                    <option value="{{ $value->namaBidang }}">{{ $value->namaBidang }}</option>
                @endforeach
            </select>
        </div>
        <div class="mb-3">
            <label for="gaji" class="form-label">Gaji</label>
            <input type="number" class="form-control" name="gaji">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection

@extends('layout.admin.master')

@section('header')
    <strong>Data Pelatihan Karyawan</strong>
@endsection

@section('judul')
    <h5>Tabel Data Pelatihan Karyawan</h5>
@endsection

@section('form')
    <div class="table-responsive">
        <table class="table table-hover" id="dataTable">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama Pelatihan</th>
                    <th scope="col">Nama Penyelenggara</th>
                    <th scope="col">Keahlian</th>
                    <th scope="col">Tahun Pelatihan</th>
                    <th scope="col">Nama Karyawan</th>
                    <th scope="col">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($pelatihan as $key => $value)
                    <tr>
                        <th>{{ $key + 1 }}</th>
                        <td>{{ $value->namaPelatihan }}</td>
                        <td>{{ $value->penyelenggara }}</td>
                        <td>{{ $value->keahlian }}</td>
                        <td>{{ $value->tahun }}</td>
                        <td>
                            {{ DB::table('users')->where('id', $value->user_id)->value('name') }}
                        </td>
                        <td>
                            <form action="/pelatihan/{{ $value->id }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <a href="/pelatihan/{{ $value->id }}/delete" class="btn btn-danger"
                                    onclick="confirmation(event)">Delete</a>
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="7" class="text-center">Tidak Ada Data.</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
@endsection

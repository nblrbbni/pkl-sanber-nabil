@extends('layout.admin.master')

@section('header')
    <strong>Data Karyawan</strong>
@endsection

@section('judul')
    <h5>Tabel Data Karyawan</h5>
@endsection

@section('form')
    <div class="table-responsive">
        <table class="table table-hover" id="dataTable">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Email</th>
                    <th scope="col">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($users as $key => $value)
                    @if($value->name !== 'Admin')
                        <tr>
                            <th>{{ $key + 1 }}</th>
                            <td>{{ $value->name }}</td>
                            <td>{{ $value->email  }}</td>
                            <td>
                                <form action="/karyawan/{{ $value->id }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <a href="/karyawan/{{ $value->id }}/delete" class="btn btn-danger"
                                        onclick="confirmation(event)">Delete</a>
                                </form>
                            </td>
                        </tr>
                    @endif
                @empty
                    <tr>
                        <td colspan="5" class="text-center">Tidak Ada Data.</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
@endsection

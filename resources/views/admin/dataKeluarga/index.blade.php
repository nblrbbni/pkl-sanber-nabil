@extends('layout.admin.master')

@section('header')
    <strong>Data Keluarga Karyawan</strong>
@endsection

@section('judul')
    <h5>Tabel Data Keluarga Karyawan</h5>
@endsection

@section('form')
    <div class="table-responsive">
        <table class="table table-hover" id="dataTable">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama Anggota Keluarga</th>
                    <th scope="col">Jenis Kelamin</th>
                    <th scope="col">Status di dalam Keluarga</th>
                    <th scope="col">Nama Karyawan</th>
                    <th scope="col">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($keluarga as $key => $value)
                    <tr>
                        <th>{{ $key + 1 }}</th>
                        <td>{{ $value->namaanggota }}</td>
                        <td>{{ $value->jeniskelamin }}</td>
                        <td>{{ $value->status }}</td>
                        <td>
                            {{ DB::table('users')->where('id', $value->user_id)->value('name') }}
                        </td>
                        <td>
                            <form action="/keluarga/{{ $value->id }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <a href="/keluarga/{{ $value->id }}/delete" class="btn btn-danger"
                                    onclick="confirmation(event)">Delete</a>
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="6" class="text-center">Tidak Ada Data.</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
@endsection

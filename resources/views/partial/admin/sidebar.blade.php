            <div class="sidebar-content js-simplebar">
                <a class="sidebar-brand" href="index.html">
                    <span class="align-middle">Admin Dashboard</span>
                </a>

                <ul class="sidebar-nav">
                    <li class="sidebar-item">
                        <a class="sidebar-link" href="/">
                            <i class="align-middle" data-feather="home"></i> <span class="align-middle">Dashboard</span>
                        </a>
                    </li>

                    <li class="sidebar-header">
                        Data
                    </li>

                     <li class="sidebar-item {{ 'karyawan' == request()->path() ? 'active' : '' }}">
                        <a class="sidebar-link" href="/karyawan">
                            <i class="align-middle fas fa-users"></i>
                            <!-- Menggunakan ikon users dari Font Awesome 5 -->
                            <span class="align-middle">Employees</span>
                        </a>
                    </li>

                    <li class="sidebar-item {{ 'gaji' == request()->path() ? 'active' : '' }}">
                        <a class="sidebar-link" href="/gaji">
                            <i class="align-middle fas fa-money-bill"></i>
                            <!-- Menggunakan ikon dollar-sign dari Font Awesome 5 -->
                            <span class="align-middle">Salaries</span>
                        </a>
                    </li>

                    <li class="sidebar-item {{ 'keluarga/show' == request()->path() ? 'active' : '' }}">
                        <a class="sidebar-link" href="/keluarga/show">
                            <i class="align-middle fas fa-layer-group"></i>
                            <!-- Menggunakan ikon layers dari Font Awesome 5 -->
                            <span class="align-middle">Family Backgrounds</span>
                        </a>
                    </li>

                    <li class="sidebar-item {{ 'pendidikan/show' == request()->path() ? 'active' : '' }}">
                        <a class="sidebar-link" href="/pendidikan/show">
                            <i class="align-middle fas fa-school"></i>
                            <!-- Menggunakan ikon layers dari Font Awesome 5 -->
                            <span class="align-middle">Education</span>
                        </a>
                    </li>

                    <li class="sidebar-item {{ 'pekerjaan/show' == request()->path() ? 'active' : '' }}">
                        <a class="sidebar-link" href="/pekerjaan/show">
                            <i class="align-middle fas fa-suitcase"></i>
                            <!-- Menggunakan ikon layers dari Font Awesome 5 -->
                            <span class="align-middle">Jobdesk</span>
                        </a>
                    </li>

                    <li class="sidebar-item {{ 'pelatihan/show' == request()->path() ? 'active' : '' }}">
                        <a class="sidebar-link" href="/pelatihan/show">
                            <i class="align-middle fas fa-box-archive"></i>
                            <!-- Menggunakan ikon layers dari Font Awesome 5 -->
                            <span class="align-middle">Trainings</span>
                        </a>
                    </li>

                    <li class="sidebar-item {{ 'sertifikasi/show' == request()->path() ? 'active' : '' }}">
                        <a class="sidebar-link" href="/sertifikasi/show">
                            <i class="align-middle fas fa-certificate"></i>
                            <!-- Menggunakan ikon layers dari Font Awesome 5 -->
                            <span class="align-middle">Certifications</span>
                        </a>
                    </li>

                    <li class="sidebar-header">
                        Settings
                    </li>

                    <li class="sidebar-item {{ 'bidang' == request()->path() ? 'active' : '' }}">
                        <a class="sidebar-link" href="/bidang">
                            <i class="align-middle fas fa-bars"></i>
                            <!-- Menggunakan ikon layers dari Font Awesome 5 -->
                            <span class="align-middle">Job Field</span>
                        </a>
                    </li>

                    <li class="sidebar-item {{ 'profile-admin' == request()->path() ? 'active' : '' }}">
                        <a class="sidebar-link" href="/profile-admin">
                            <i class="align-middle fas fa-user"></i> <span class="align-middle">Profile</span>
                        </a>
                    </li>
                    {{--
                    <li class="sidebar-item">
                        <a class="sidebar-link" href="pages-blank.html">
                            <i class="align-middle" data-feather="book"></i> <span class="align-middle">Blank</span>
                        </a>
                    </li>

                    <li class="sidebar-header">
                        Tools & Components
                    </li>

                    <li class="sidebar-item">
                        <a class="sidebar-link" href="ui-buttons.html">
                            <i class="align-middle" data-feather="square"></i> <span class="align-middle">Buttons</span>
                        </a>
                    </li>

                    <li class="sidebar-item">
                        <a class="sidebar-link" href="ui-forms.html">
                            <i class="align-middle" data-feather="check-square"></i> <span
                                class="align-middle">Forms</span>
                        </a>
                    </li>

                    <li class="sidebar-item">
                        <a class="sidebar-link" href="ui-cards.html">
                            <i class="align-middle" data-feather="grid"></i> <span class="align-middle">Cards</span>
                        </a>
                    </li>

                    <li class="sidebar-item">
                        <a class="sidebar-link" href="ui-typography.html">
                            <i class="align-middle" data-feather="align-left"></i> <span
                                class="align-middle">Typography</span>
                        </a>
                    </li>

                    <li class="sidebar-item">
                        <a class="sidebar-link" href="icons-feather.html">
                            <i class="align-middle" data-feather="coffee"></i> <span class="align-middle">Icons</span>
                        </a>
                    </li>

                    <li class="sidebar-header">
                        Plugins & Addons
                    </li>

                    <li class="sidebar-item">
                        <a class="sidebar-link" href="charts-chartjs.html">
                            <i class="align-middle" data-feather="bar-chart-2"></i> <span
                                class="align-middle">Charts</span>
                        </a>
                    </li>

                    <li class="sidebar-item">
                        <a class="sidebar-link" href="maps-google.html">
                            <i class="align-middle" data-feather="map"></i> <span class="align-middle">Maps</span>
                        </a>
                    </li> --}}
                </ul>
            </div>

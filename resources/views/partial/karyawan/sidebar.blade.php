            <nav class="sidebar sidebar-offcanvas" id="sidebar">
                <ul class="nav">
                    <li class="nav-item">
                        <a class="nav-link" href="/">
                            <span class="menu-title">Dashboard</span>
                            <i class="mdi mdi-home menu-icon"></i>
                        </a>
                    </li>
                    <div class="border-bottom"></div>
                    <li class="nav-item">
                        <a class="nav-link" href="/keluarga">
                            <span class="menu-title">Family Background</span>
                            <i class="mdi mdi-account-group menu-icon"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/pendidikan">
                            <span class="menu-title">Educations</span>
                            <i class="mdi mdi-school menu-icon"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/pekerjaan">
                            <span class="menu-title">Jobdesk</span>
                            <i class="mdi mdi-account-details menu-icon"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/pelatihan">
                            <span class="menu-title">Trainings</span>
                            <i class="mdi mdi-account-plus menu-icon"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/sertifikasi">
                            <span class="menu-title">Certifications</span>
                            <i class="mdi mdi-certificate menu-icon"></i>
                        </a>
                    </li>
                    <div class="border-bottom"></div>
                    <li class="nav-item">
                        <a class="nav-link" href="/profile-karyawan">
                            <span class="menu-title">Profile</span>
                            <i class="mdi mdi-account menu-icon"></i>
                        </a>
                    </li>
                </ul>
            </nav>

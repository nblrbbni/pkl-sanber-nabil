@extends('layout.karyawan.master')

@section('form')
    <form action="/pelatihan/{{ $pelatihan->id }}" method="POST">
        @csrf
        @method('PUT')
        <div class="mb-3">
            <label for="namaPelatihan" class="form-label">Nama Pelatihan</label>
            <input type="text" class="form-control" name="namaPelatihan" id="namaPelatihan"
                value="{{ $pelatihan->namaPelatihan }}">
        </div>
        <div class="mb-3">
            <label for="penyelenggara" class="form-label">Nama Penyelenggara</label>
            <input type="text" class="form-control" name="penyelenggara" id="penyelenggara"
                value="{{ $pelatihan->penyelenggara }}">
        </div>
        <div class="mb-3">
            <label for="keahlian" class="form-label">Keahlian</label>
            <textarea class="form-control" name="keahlian" id="keahlian">{{ $pelatihan->keahlian }}</textarea>
        </div>
        <div class="mb-3">
            <label for="tahun" class="form-label">Tahun Pelatihan</label>
            <input type="date" name="tahun" class="form-control" value="<?php echo date('Y-m-d'); ?>"
                value="{{ $pelatihan->tahun }}">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
        <a href="/pelatihan" class="btn btn-danger">Back</a>
    </form>
@endsection

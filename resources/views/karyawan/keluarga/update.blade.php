@extends('layout.karyawan.master')

@section('form')
    <form action="/keluarga/{{ $keluarga->id }}" method="POST">
        @csrf
        @method('PUT')
        <div class="mb-3">
            <label for="namaanggota" class="form-label">Nama Anggota Keluarga</label>
            <input type="text" class="form-control" name="namaanggota" id="namaanggota" value="{{ $keluarga->namaanggota }}"
                required>
        </div>
        <div class="mb-3">
            <label for="jenis kelamin" class="form-label">Jenis Kelamin</label>
            <select class="form-control form-control-sm" name="jeniskelamin" id="jeniskelamin">
                <option value="Laki-laki">Laki-laki</option>
                <option value="Laki-laki">Perempuan</option>
            </select>
        </div>
        <div class="mb-3">
            <label for="status" class="form-label">Status di dalam Keluarga</label>
            <input type="text" class="form-control" name="status" id="status" value="{{ $keluarga->status }}"
                required>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
        <a href="/keluarga" class="btn btn-danger">Back</a>
    </form>
@endsection

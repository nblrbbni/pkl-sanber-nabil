@extends('layout.karyawan.master')


@section('judul')
    Data Keluarga
@endsection

@section('form')
    <form action="/keluarga" method="POST" class="mb-3">
        @csrf
        <div class="mb-3">
            <label for="namaanggota" class="form-label">Nama Anggota Keluarga</label>
            <input type="text" class="form-control" name="namaanggota" id="namaanggota" required>
        </div>
        <div class="mb-3">
            <label for="jenis kelamin" class="form-label">Jenis Kelamin</label>
            <select class="form-control form-control-sm" name="jeniskelamin" id="jeniskelamin" required>
                <option value="Laki-laki">Laki-laki</option>
                <option value="Laki-laki">Perempuan</option>
            </select>
        </div>
        <div class="mb-3">
            <label for="status" class="form-label">Status di dalam Keluarga</label>
             <select class="form-control form-control-sm" name="status" id="status" required>
                <option value="Ayah">Ayah</option>
                <option value="Ibu">Ibu</option>
                <option value="Anak">Anak</option>
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection

@section('content')
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama Anggota Keluarga</th>
                    <th scope="col">Jenis Kelamin</th>
                    <th scope="col">Status di dalam Keluarga</th>
                    <th scope="col">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($keluarga as $key => $value)
                    <tr>
                        <th>{{ $key + 1 }}</th>
                        <td>{{ $value->namaanggota }}</td>
                        <td>{{ $value->jeniskelamin }}</td>
                        <td>{{ $value->status }}</td>
                        <td>
                            <form action="/keluarga/{{ $value->id }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <a href="/keluarga/{{ $value->id }}/edit" class="btn btn-warning"><i class="fas fa-edit"></i></a>
                                <a href="/keluarga/{{ $value->id }}/delete" class="btn btn-danger"
                                    onclick="confirmation(event)"><i class="fas fa-trash-alt"></i></a>
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5" class="text-center">Tidak Ada Data.</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
@endsection

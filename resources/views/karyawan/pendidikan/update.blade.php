@extends('layout.karyawan.master')

@section('form')
    <form action="/pendidikan/{{ $pendidikan->id }}" method="POST">
        @csrf
        @method('PUT')
        <div class="mb-3">
            <label for="sd" class="form-label">Sekolah Dasar</label>
            <input type="text" class="form-control" name="sd" id="sd" value="{{ $pendidikan->sd }}">
        </div>
        <div class="mb-3">
            <label for="smp" class="form-label">Sekolah Menengah Pertama</label>
            <input type="text" class="form-control" name="smp" id="smp"  value="{{ $pendidikan->smp }}">
        </div>
        <div class="mb-3">
            <label for="sma" class="form-label">Sekolah Menengah Atas</label>
            <input type="text" class="form-control" name="sma" id="sma" value="{{ $pendidikan->sma }}">
        </div>
        <div class="mb-3">
            <label for="kuliah" class="form-label">Universitas</label>
            <input type="text" class="form-control" name="kuliah" id="kuliah" value="{{ $pendidikan->kuliah }}">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
        <a href="/pendidikan" class="btn btn-danger">Back</a>
    </form>
@endsection

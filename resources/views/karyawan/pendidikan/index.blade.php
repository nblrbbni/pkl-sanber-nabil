@extends('layout.karyawan.master')

@section('judul')
    Data Pendidikan
@endsection

@section('form')
    <form action="/pendidikan" method="POST">
        @csrf
        <div class="mb-3">
            <label for="sd" class="form-label">Sekolah Dasar</label>
            <input type="text" class="form-control" name="sd" id="sd" required>
        </div>
        <div class="mb-3">
            <label for="smp" class="form-label">Sekolah Menengah Pertama</label>
            <input type="text" class="form-control" name="smp" id="smp">
        </div>
        <div class="mb-3">
            <label for="sma" class="form-label">Sekolah Menengah Atas</label>
            <input type="text" class="form-control" name="sma" id="sma">
        </div>
        <div class="mb-3">
            <label for="kuliah" class="form-label">Universitas</label>
            <input type="text" class="form-control" name="kuliah" id="kuliah">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection

@section('content')
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Sekolah Dasar</th>
                    <th scope="col">Sekolah Menengah Pertama</th>
                    <th scope="col">Sekolah Menengah Atas</th>
                    <th scope="col">Universitas</th>
                    <th scope="col">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($pendidikan as $key => $value)
                    <tr>
                        <th>{{ (int) $key + 1 }}</th>
                        <td>{{ $value->sd }}</td>
                        <td>{{ $value->smp }}</td>
                        <td>{{ $value->sma }}</td>
                        <td>{{ $value->kuliah }}</td>
                        <td>
                            <form action="/pendidikan/{{ $value->id }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <a href="/pendidikan/{{ $value->id }}/edit"class="btn btn-warning"><i class="fas fa-edit"></i></a>
                                <a href="/pendidikan/{{ $value->id }}/delete" class="btn btn-danger"
                                    onclick="confirmation(event)"><i class="fas fa-trash-alt"></i></a>
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="6" class="text-center">Tidak Ada Data.</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
@endsection

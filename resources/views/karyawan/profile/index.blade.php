@extends('layout.karyawan.master')

@section('judul')
    Data Diri
@endsection

@section('form')
    <form action="/profile-karyawan/update" method="POST">
        @csrf
        <div class="mb-3">
            <label for="name" class="form-label">Nama Karyawan</label>
            <input type="text" class="form-control" name="name" id="name" value="{{ $user->name }}">
        </div>
        <div class="mb-3">
            <label for="email" class="form-label">Email Penyelenggara</label>
            <input type="text" class="form-control" name="email" id="email" value="{{ $user->email }}">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection

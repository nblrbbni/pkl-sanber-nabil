@extends('layout.karyawan.master')

@section('form')
    <form action="/pekerjaan/{{ $pekerjaan->id }}" method="POST">
        @csrf
        @method('PUT')
        <div class="mb-3">
            <label for="namaKaryawan" class="form-label">Nama Karyawan</label>
            <input type="text" class="form-control" name="namaKaryawan" value="{{ $pekerjaan->namaKaryawan }}" readonly>
        </div>
        <div class="mb-3">
            <label for="namaPekerjaan" class="form-label">Nama Pekerjaan</label>
            <select name="namaPekerjaan" id="namaPekerjaan" class="form-control" required>
                @foreach ($bidang as $value)
                    <option value="{{ $value->namaBidang }}" {{ $value->namaBidang == $pekerjaan->namaPekerjaan ? 'selected' : '' }}>
                        {{ $value->namaBidang }}
                    </option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
        <a href="/pekerjaan" class="btn btn-danger">Back</a>
    </form>
@endsection

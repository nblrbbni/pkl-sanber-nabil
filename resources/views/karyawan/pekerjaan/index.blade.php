@extends('layout.karyawan.master')

@section('judul')
    Data Pekerjaan
@endsection

@section('form')
    <form action="/pekerjaan" method="POST">
        @csrf
        <div class="mb-3">
            <label for="namaKaryawan" class="form-label">Nama Karyawan</label>
            <input type="text" class="form-control" name="namaKaryawan" value="{{ Auth::user()->name }}" readonly>
        </div>
        <div class="mb-3">
            <label for="namaPekerjaan" class="form-label">Nama Pekerjaan</label>
            <select name="namaPekerjaan" id="namaPekerjaan" class="form-control form-control-sm" required>
                @foreach ($bidang as $value)
                    <option value="{{ $value->namaBidang }}">{{ $value->namaBidang }}</option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection

@section('content')
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama Karyawan</th>
                    <th scope="col">Nama Pekerjaan</th>
                    <th scope="col">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($pekerjaan as $key => $value)
                    <tr>
                        <th>{{ $key + 1 }}</th>
                        <td>{{ $value->namaKaryawan }}</td>
                        <td>{{ $value->namaPekerjaan }}</td>
                        <td>
                            <form action="/pekerjaan/{{ $value->id }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <a href="/pekerjaan/{{ $value->id }}/edit" class="btn btn-warning"><i class="fas fa-edit"></i></a>
                                <a href="/pekerjaan/{{ $value->id }}/delete" class="btn btn-danger"
                                    onclick="confirmation(event)"><i class="fas fa-trash-alt"></i></a>
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="3" class="text-center">Tidak Ada Data.</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
@endsection

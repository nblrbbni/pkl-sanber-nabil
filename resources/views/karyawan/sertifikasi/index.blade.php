@extends('layout.karyawan.master')

@section('judul')
    Data Sertifikasi
@endsection

@section('form')
    <form action="/sertifikasi" method="POST">
        @csrf
        <div class="mb-3">
            <label for="sertifikat" class="form-label">Sertifikast</label>
            <input type="file" id="images" accept="image/*" name="sertifikat" class="form-control" required>
        </div>
        <div class="mb-3">
            <label for="penyelenggara" class="form-label">Nama Penyelenggara</label>
            <input type="text" class="form-control" name="penyelenggara" id="penyelenggara" required>
        </div>
        <div class="mb-3">
            <label for="tahun" class="form-label">Tahun Pelatihan</label>
            <input type="date" name="tahun" class="form-control" value="<?php echo date('Y-m-d'); ?>">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection

@section('content')
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Sertifikat</th>
                    <th scope="col">Nama Penyelenggara</th>
                    <th scope="col">Tahun Sertifikat</th>
                    <th scope="col">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($sertifikasi as $key => $value)
                    <tr>
                        <th>{{ $key + 1 }}</th>
                        <td>{{ $value->sertifikat }}</td>
                        <td>{{ $value->penyelenggara }}</td>
                        <td>{{ $value->tahun }}</td>
                        <td>
                            <form action="/sertifikasi/{{ $value->id }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <a href="/sertifikasi/{{ $value->id }}/edit" class="btn btn-warning"><i class="fas fa-edit"></i></a>
                                <a href="/sertifikasi/{{ $value->id }}/delete" class="btn btn-danger"
                                    onclick="confirmation(event)"><i class="fas fa-trash-alt"></i></a>
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5" class="text-center">Tidak Ada Data.</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
@endsection

@extends('layout.karyawan.master')

@section('form')
    <form action="/sertifikasi/{{ $sertifikasi->id }}" method="POST">
        @csrf
        @method('PUT')
        <div class="mb-3">
            <label for="sertifikat" class="form-label">Sertifikat</label>
            <input type="file" id="images" accept="image/*" name="sertifikat" class="form-control" value="{{ $sertifikasi->sertifikat }}">
        </div>
        <div class="mb-3">
            <label for="penyelenggara" class="form-label">Nama Penyelenggara</label>
            <input type="text" class="form-control" name="penyelenggara" id="penyelenggara" value="{{ $sertifikasi->penyelenggara }}">
        </div>
        <div class="mb-3">
            <label for="tahun" class="form-label">Tahun Sertifikat</label>
            <input type="date" name="tahun" class="form-control" value="<?php echo date('Y-m-d'); ?>" value="{{ $sertifikasi->tahun }}">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
        <a href="/sertifikasi" class="btn btn-danger">Back</a>
    </form>
@endsection

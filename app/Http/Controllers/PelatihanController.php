<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class PelatihanController extends Controller
{
    public function index()
    {
        $pelatihan = DB::table('pelatihan')
            ->where('user_id', Auth::id())
            ->get();

        return view('karyawan.pelatihan.index', compact('pelatihan'));
    }

    public function show()
    {
        $pelatihan = DB::table('pelatihan')->get();

        return view('admin.dataPelatihan.index', [
            'pelatihan' => $pelatihan,
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'namaPelatihan' => 'required',
            'penyelenggara' => 'required',
            'keahlian' => 'required',
            'tahun' => 'required',
        ]);

        DB::table('pelatihan')->insert([
            'namaPelatihan' => $request['namaPelatihan'],
            'penyelenggara' => $request['penyelenggara'],
            'keahlian' => $request['keahlian'],
            'tahun' => $request['tahun'],
            'user_id' => Auth::id(),
        ]);

        Alert::success('Success!', 'Data created successfully!');
        return redirect('/pelatihan');
    }

    public function edit($id)
    {
        $pelatihan = DB::table('pelatihan')->where('id', $id)->first();

        return view('karyawan.pelatihan.update', ['pelatihan' => $pelatihan]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'namaPelatihan' => 'required',
            'penyelenggara' => 'required',
            'keahlian' => 'required',
            'tahun' => 'required',
        ]);

        DB::table('pelatihan')
            ->where('id', $id)
            ->update([
                'namaPelatihan' => $request['namaPelatihan'],
                'penyelenggara' => $request['penyelenggara'],
                'keahlian' => $request['keahlian'],
                'tahun' => $request['tahun'],
                'user_id' => Auth::id(),
            ]);

        Alert::success('Success!', 'Data updated successfully!');
        return redirect('/pelatihan');
    }

    public function delete($id)
    {
        DB::table('pelatihan')->where('id', $id)->delete();

        Alert::success('Success!', 'Data successfully deleted!');
        return redirect('/pelatihan');
    }
}

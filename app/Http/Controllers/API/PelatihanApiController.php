<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class APIController extends Controller
{
    public function index()
    {
        $pelatihan = DB::table('pelatihan')
            ->where('user_id', Auth::id())
            ->get();

        return response()->json($pelatihan, Response::HTTP_OK);
    }

    public function store(Request $request)
    {
        $request->validate([
            'namaPelatihan' => 'required',
            'penyelenggara' => 'required',
            'keahlian' => 'required',
            'tahun' => 'required',
        ]);

        $pelatihan = DB::table('pelatihan')->insertGetId([
            'namaPelatihan' => $request['namaPelatihan'],
            'penyelenggara' => $request['penyelenggara'],
            'keahlian' => $request['keahlian'],
            'tahun' => $request['tahun'],
            'user_id' => Auth::id(),
        ]);

        return response()->json(['id' => $pelatihan], Response::HTTP_CREATED);
    }
}

<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class PekerjaanApiController extends Controller
{
    public function index()
    {
        $pekerjaan = DB::table('pekerjaan')
            ->where('user_id', Auth::id())
            ->get();

        return response()->json(['pekerjaan' => $pekerjaan], 200);
    }

    public function store(Request $request)
    {
        $request->validate([
            'namaPekerjaan' => 'required',
        ]);

        $pekerjaan = DB::table('pekerjaan')->insertGetId([
            'namaPekerjaan' => $request['namaPekerjaan'],
            'user_id' => Auth::id(),
        ]);

        return response()->json(['pekerjaan' => $pekerjaan], 201);
    }

    public function show($id)
    {
        $pekerjaan = DB::table('pekerjaan')->find($id);

        if (!$pekerjaan) {
            return response()->json(['message' => 'Not Found'], 404);
        }

        return response()->json(['pekerjaan' => $pekerjaan], 200);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'namaPekerjaan' => 'required',
        ]);

        $affected = DB::table('pekerjaan')
            ->where('id', $id)
            ->where('user_id', Auth::id())
            ->update([
                'namaPekerjaan' => $request['namaPekerjaan'],
            ]);

        if ($affected == 0) {
            return response()->json(['message' => 'Not Found or Unauthorized'], 404);
        }

        return response()->json(['message' => 'Updated successfully'], 200);
    }

    public function destroy($id)
    {
        $affected = DB::table('pekerjaan')
            ->where('id', $id)
            ->where('user_id', Auth::id())
            ->delete();

        if ($affected == 0) {
            return response()->json(['message' => 'Not Found or Unauthorized'], 404);
        }

        return response()->json(['message' => 'Deleted successfully'], 200);
    }
}

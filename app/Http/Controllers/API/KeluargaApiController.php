<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class KeluargaApiController extends Controller
{
    public function index()
    {
        $keluarga = DB::table('keluarga')
            ->where('user_id', Auth::id())
            ->get();

        return response()->json(['keluarga' => $keluarga], 200);
    }

    public function store(Request $request)
    {
        $request->validate([
            'namaanggota' => 'required',
            'jeniskelamin' => 'required',
            'status' => 'required',
        ]);

        Log::info('Data yang Diterima:', $request->all());

        $keluarga = DB::table('keluarga')->insertGetId([
            'namaanggota' => $request->namaanggota,
            'jeniskelamin' => $request->jeniskelamin,
            'status' => $request->status,
            'user_id' => Auth::id(),
        ]);

        Log::info('ID Keluarga yang Disimpan:', $keluarga);

        return response()->json(['keluarga' => $keluarga], 201);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function indexKaryawan()
    {
        $user = Auth::user();

        return view('karyawan.profile.index', [
            'user' => $user,
        ]);
    }

    public function updateKaryawan(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
        ]);

        $user = auth()->user();
        $user->name = $request->input('name');
        $user->email = $request->input('email');

        if ($request->filled('password')) {
            $user->password = bcrypt($request->input('password'));
        }

        $user->save();

        return redirect('/profile-karyawan');
    }

    public function indexAdmin()
    {
        $user = Auth::user();

        return view('admin.profile.index', [
            'user' => $user,
        ]);
    }

    public function updateAdmin(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
        ]);

        $user = auth()->user();
        $user->name = $request->input('name');
        $user->email = $request->input('email');

        if ($request->filled('password')) {
            $user->password = bcrypt($request->input('password'));
        }

        $user->save();

        return redirect('/profile-admin');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class PendidikanController extends Controller
{
    public function index()
    {
        $pendidikan = DB::table('pendidikan')
            ->where('user_id', Auth::id())
            ->get();

        return view('karyawan.pendidikan.index', compact('pendidikan'));
    }

    public function show()
    {
        $pendidikan = DB::table('pendidikan')->get();

        return view('admin.dataPendidikan.index', [
            'pendidikan' => $pendidikan,
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'sd' => 'nullable|string|max:50',
            'smp' => 'nullable|string|max:50',
            'sma' => 'nullable|string|max:50',
            'kuliah' => 'nullable|string|max:50',
        ]);

        DB::table('pendidikan')->insert([
            'sd' => $request->input('sd'),
            'smp' => $request->input('smp'),
            'sma' => $request->input('sma'),
            'kuliah' => $request->input('kuliah'),
            'user_id' => Auth::id(),
        ]);

        Alert::success('Success!', 'Data created successfully!');
        return redirect('/pendidikan');
    }


    public function edit($id)
    {
        $pendidikan = DB::table('pendidikan')->where('id', $id)->first();

        return view('karyawan.pendidikan.update', ['pendidikan' => $pendidikan]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'sd' => 'nullable|string|max:50',
            'smp' => 'nullable|string|max:50',
            'sma' => 'nullable|string|max:50',
            'kuliah' => 'nullable|string|max:50',
        ]);

        DB::table('pendidikan')
            ->where('id', $id)
            ->update([
                'sd' => $request->input('sd'),
                'smp' => $request->input('smp'),
                'sma' => $request->input('sma'),
                'kuliah' => $request->input('kuliah'),
            ]);

        Alert::success('Success!', 'Data updated successfully!');
        return redirect('/pendidikan');
    }

    public function delete($id)
    {
        DB::table('pendidikan')->where('id', $id)->delete();

        Alert::success('Success!', 'Data successfully deleted!');
        return redirect('/pendidikan');
    }
}

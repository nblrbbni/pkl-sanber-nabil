<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KaryawanController extends Controller
{
    public function index()
    {
        $users = DB::table('users')->get();

        return view('admin.dataKaryawan.index', [
            'users' => $users,
        ]);
    }
}

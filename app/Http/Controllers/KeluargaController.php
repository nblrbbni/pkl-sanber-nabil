<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class KeluargaController extends Controller
{
    public function index()
    {
        $keluarga = DB::table('keluarga')
        ->where('user_id', Auth::id())
            ->get();

        return view('karyawan.keluarga.index', compact('keluarga'));
    }

    public function show()
    {
        $keluarga = DB::table('keluarga')->get();

        return view('admin.dataKeluarga.index', [
            'keluarga' => $keluarga,
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'namaanggota' => 'required',
            'jeniskelamin' => 'required',
            'status' => 'required',
        ]);

        DB::table('keluarga')->insert([
            'namaanggota' => $request['namaanggota'],
            'jeniskelamin' => $request['jeniskelamin'],
            'status' => $request['status'],
            'user_id' => Auth::id(),
        ]);

        Alert::success('Success!', 'Data created successfully!');
        return redirect('/keluarga');
    }

    public function edit($id)
    {
        $keluarga = DB::table('keluarga')->where('id', $id)->first();

        return view('karyawan.keluarga.update', ['keluarga' => $keluarga]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'namaanggota' => 'required',
            'jeniskelamin' => 'required',
            'status' => 'required',
        ]);

        DB::table('keluarga')
            ->where('id', $id)
            ->update(
                [
                    'namaanggota' => $request->namaanggota,
                    'jeniskelamin' => $request->jeniskelamin,
                    'status' => $request->status,
                'user_id' => Auth::id(),
                ],
            );

        Alert::success('Success!', 'Data updated successfully');
        return redirect('/keluarga');
    }

    public function delete($id)
    {
        DB::table('keluarga')->where('id', $id)->delete();

        Alert::success('Success!', 'Data successfully deleted!');
        return redirect('/keluarga');
    }
}

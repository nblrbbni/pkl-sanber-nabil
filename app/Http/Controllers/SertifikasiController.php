<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class SertifikasiController extends Controller
{
    public function index()
    {
        $sertifikasi = DB::table('sertifikasi')
            ->where('user_id', Auth::id())
            ->get();

        return view('karyawan.sertifikasi.index', compact('sertifikasi'));
    }

    public function show()
    {
        $sertifikasi = DB::table('sertifikasi')->get();

        return view('admin.dataSertifikasi.index', [
            'sertifikasi' => $sertifikasi,
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'sertifikat' => 'required',
            'penyelenggara' => 'required',
            'tahun' => 'required',
        ]);

        DB::table('sertifikasi')->insert([
            'sertifikat' => $request['sertifikat'],
            'penyelenggara' => $request['penyelenggara'],
            'tahun' => $request['tahun'],
            'user_id' => Auth::id(),
        ]);

        Alert::success('Success!', 'Data created successfully!');
        return redirect('/sertifikasi');
    }

    public function edit($id)
    {
        $sertifikasi = DB::table('sertifikasi')->where('id', $id)->first();

        return view('karyawan.sertifikasi.update', ['sertifikasi' => $sertifikasi]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'sertifikat' => 'required',
            'penyelenggara' => 'required',
            'tahun' => 'required',
        ]);

        DB::table('sertifikasi')
            ->where('id', $id)
            ->update([
                'sertifikat' => $request['sertifikat'],
                'penyelenggara' => $request['penyelenggara'],
                'tahun' => $request['tahun'],
                'user_id' => Auth::id(),
            ]);

        Alert::success('Success!', 'Data updated successfully!');
        return redirect('/sertifikasi');
    }

    public function delete($id)
    {
        DB::table('sertifikasi')->where('id', $id)->delete();

        Alert::success('Success!', 'Data successfully deleted!');
        return redirect('/sertifikasi');
    }
}

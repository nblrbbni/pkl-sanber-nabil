<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class BidangController extends Controller
{
    public function index()
    {
        $bidang = DB::table('bidang')->get();

        return view('admin.bidang.index', [
            'bidang' => $bidang,
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'namaBidang' => 'required',
        ]);

        DB::table('bidang')->insert([
            'namaBidang' => $request['namaBidang'],
        ]);

        Alert::success('Success!', 'Data created successfully!');
        return redirect('/bidang');
    }

    public function edit($id)
    {
        $bidang = DB::table('bidang')->where('id', $id)->first();

        return view('admin.bidang.update', ['bidang' => $bidang]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'namaBidang' => 'required',
        ]);

        DB::table('bidang')
            ->where('id', $id)
            ->update(
                [
                    'namaBidang' => $request->namaBidang,
                ],
            );

        Alert::success('Success!', 'Data updated successfully');
        return redirect('/bidang');
    }

    public function delete($id)
    {
        DB::table('bidang')->where('id', $id)->delete();

        Alert::success('Success!', 'Data successfully deleted!');
        return redirect('/bidang');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class PekerjaanController extends Controller
{
    public function index()
    {
        $pekerjaan = DB::table('pekerjaan')
            ->where('user_id', Auth::id())
            ->get();
        $bidang = DB::table('bidang')->get();

        return view('karyawan.pekerjaan.index', [
            'pekerjaan' => $pekerjaan,
            'bidang' => $bidang,
        ]);
    }

    public function show()
    {
        $pekerjaan = DB::table('pekerjaan')->get();

        return view('admin.dataPekerjaan.index', [
            'pekerjaan' => $pekerjaan,
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'namaKaryawan' => 'required',
            'namaPekerjaan' => 'required',
        ]);

        DB::table('pekerjaan')->insert([
            'namaKaryawan' => $request['namaKaryawan'],
            'namaPekerjaan' => $request['namaPekerjaan'],
            'user_id' => Auth::id(),
            'gaji' => 0,
        ]);

        Alert::success('Success!', 'Data created successfully!');
        return redirect('/pekerjaan');
    }

    public function edit($id)
    {
        $pekerjaan = DB::table('pekerjaan')->where('id', $id)->first();
        $bidang = DB::table('bidang')->get();

        return view('karyawan.pekerjaan.update', [
            'pekerjaan' => $pekerjaan,
            'bidang' => $bidang,
        ]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'namaKaryawan' => 'required',
            'namaPekerjaan' => 'required',
        ]);

        DB::table('pekerjaan')
            ->where('id', $id)
            ->update([
                'namaKaryawan' => $request['namaKaryawan'],
                'namaPekerjaan' => $request['namaPekerjaan'],
                'user_id' => Auth::id(),
            ]);

        Alert::success('Success!', 'Data updated successfully!');
        return redirect('/pekerjaan');
    }

    public function delete($id)
    {
        DB::table('pekerjaan')->where('id', $id)->delete();

        Alert::success('Success!', 'Data successfully deleted!');
        return redirect('/pekerjaan');
    }

    public function gaji()
    {
        $pekerjaan = DB::table('pekerjaan')->get();

        return view('admin.gaji.index', [
            'pekerjaan' => $pekerjaan,
        ]);
    }

    public function editGaji($id)
    {
        $pekerjaan = DB::table('pekerjaan')->where('id', $id)->first();
        $bidang = DB::table('bidang')->get();

        return view('admin.gaji.update', [
            'pekerjaan' => $pekerjaan,
            'bidang' => $bidang,
        ]);
    }

    public function updateGaji(Request $request, $id)
    {
        $request->validate([
            'namaKaryawan' => 'required',
            'namaPekerjaan' => 'required',
            'gaji' => 'required',
        ]);

        DB::table('pekerjaan')
            ->where('id', $id)
            ->update([
                'namaKaryawan' => $request['namaKaryawan'],
                'namaPekerjaan' => $request['namaPekerjaan'],
                'gaji' => $request['gaji'],
                'user_id' => Auth::id(),
            ]);

        Alert::success('Success!', 'Data updated successfully!');
        return redirect('/gaji');
    }

    public function deleteGaji($id)
    {
        DB::table('pekerjaan')->where('id', $id)->delete();

        Alert::success('Success!', 'Data successfully deleted!');
        return redirect('/gaji');
    }
}
